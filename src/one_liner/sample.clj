(ns one-liner.sample
  (:require [quil.core :as q])
  (:require [clojure.core.matrix :as µ]))

(defn- calculate-pixel-ratios [width height summmed-probability [probability pixel-band]]
  (let [pixel-indices (get pixel-band :indices)
        probability-ratio (/ probability summmed-probability)
        total-pixels (* width height)
        count-pixels (µ/esum pixel-band)
        count-ratio (/ count-pixels total-pixels)
        pixel-ratio (* probability-ratio count-ratio)]
    pixel-ratio))

(defn sample [pixel-bands num-samples ]
  (println "taking" num-samples "samples from" (count pixel-bands) "bands")
  (let [num-bands (count pixel-bands)
        [width height]  (µ/shape (first pixel-bands))
        probablilities (map (fn [band] (- 1 (/ band (- num-bands 1)))) (range num-bands))
        summmed-probability (float (reduce + probablilities))
        probability-bands (partition 2 (interleave probablilities pixel-bands))
        pixel-ratios (map (partial calculate-pixel-ratios width height summmed-probability) probability-bands)
        summed-ratios (reduce + pixel-ratios)
        pixel-counts (map (fn [ratio] (int (q/map-range ratio 0 summed-ratios 0 num-samples))) pixel-ratios)
        combined (partition 2 (interleave pixel-bands pixel-counts))
        samples (map (fn [[band count]] 
                       (->> band
                            (µ/emap-indexed (fn [index value] [index value]))
                            (apply concat)
                            (filter (fn [[_ value]] (== value 1)))
                            (map (fn [[vec _]] vec))
                            (shuffle)
                            (take count))) 
                     combined)]
    (apply concat samples)))