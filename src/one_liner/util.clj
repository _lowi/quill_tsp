(ns one-liner.util
  (:require [quil.core :as q])
  (:require [clojure.core.matrix :as µ]
            [clojure.core.matrix :as µo]))


(defn- calculate-vertex [width index] (µ/matrix[(rem index width) (quot index width)]))

(defn vertices-from-indices [width height indices]
  (map #(calculate-vertex width %) indices))

(defn fast-inv-sqrt [x]
  (let [i (Float/floatToRawIntBits x)
        y (Float/intBitsToFloat (- 0x5f3759df (bit-shift-right i 1)))]
    (* y (- 1.5 (* 0.5 x y y)))))