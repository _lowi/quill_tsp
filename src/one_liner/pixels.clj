(ns one-liner.pixels
  (:require [quil.core :as q])
  (:require [clojure.core.matrix :as µ]
            [clojure.core.matrix :as µo]))

(defn- render-threshold-image
  ; applies the given threshold filter to the given image
  [image threshold]
  (let [g (q/create-graphics (.width image) (.height image))]
; side-effect: render filtered image
    (q/with-graphics g
      (q/image image 0 0)
      (apply q/display-filter [:threshold threshold]))
    g))

(defn make-threshold-matrix
  ; creates a 2d matrix (correspoding to the pixels)
  ; from a binary threshold image
  [image threshold]
  (let [threshold-image (render-threshold-image image threshold)
        px (q/pixels threshold-image)
        black (q/color 0)
        white (q/color 255)
        width (.width threshold-image)
        matrix (partition width (map (fn [color] (if (== color black) 1 0)) px))]
    matrix))



(defn initialize
  [image width height bands]
  (let [start      0.0
        end        1.0
        stepsize   (/ (- end start) bands)

        ; create the threshold-filter-levels
        ; we're taking one more as the bands are the borders of the threshold
        ; zones and there's always n+1 borders for n zones
        [h & threshold-levels] (take (+ 1 bands) (range start (+ end stepsize) stepsize))

        ; calculate the first threshold image & matrix 
        ; at 0%, ie. everything is giong to be white. or black. dunno. one color only. I'm sure of that.
        lv0-matrix (make-threshold-matrix image h)
        ]
    {:levels threshold-levels :matrices [lv0-matrix] :bands []}))

(defn calculate-band
; claculate a threshold band 
  [image band-index]
  ; (p/ (:image state) width height bands)
  
  
  
  (println "calculate-band" band-index)
  )