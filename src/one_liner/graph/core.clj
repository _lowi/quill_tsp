(ns one-liner.graph.core
  (:require [loom.graph :as loom]
            [loom.gen :as loom-gen])
  (:require [clojure.core.matrix :as µ]
            [clojure.core.matrix :as µo]))

(defn- make-edge [v0 v1] [v0 v1 (int(µo/distance v0 v1))] )


(defn- calculate-edges 
  ; calculates all edges of the graph
  ; remember that we're dealing with a complete symmetric graph
  ; (making its adjacency matrix is symmetric as well)
  ; This means we need to calculate only half of the values.
  [vertices]
  (let [[h & t] vertices
        h-edges (map (partial make-edge h) t)]
    (if (empty? t)
      h-edges
      (concat h-edges (calculate-edges t)))))

(defn make-graph
  ; Generate a complete metric TSP graph.
  [vertices]
  (let [edges (calculate-edges vertices)
        graph (loom/weighted-graph)]
    (-> graph
        (loom/add-nodes* vertices)
        (loom/add-edges* edges))))

(defn calculate-tsp-path [vertices]
  (println "calculate shortest path" (count vertices))
  (let [graph (make-graph vertices)]
    (println "graph\n" graph)
    ))