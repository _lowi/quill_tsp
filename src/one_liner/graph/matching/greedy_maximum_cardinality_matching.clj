(ns one-liner.graph.matching.greedy-maximum-cardinality-matching
  (:require [clojure.core.matrix :as µ]
            [clojure.core.matrix :as µo])
  (:require [loom.graph :as loom]))

(defn- node-edges [graph node] (get-in graph [:adj node]))

(defn- match-unsorted [nodes graph match-graph weight]
  (if (empty? nodes)
    match-graph ; iff the list of nodes is empty, return the match-graph
    (let [[node & tail] nodes ; else…
          edge (->> node
                    (node-edges graph) ; extract the edges
                    (sort-by second) ; sort by their weight
                    (remove (fn [[n _]] (contains? (:nodeset match-graph) n))) ; remove the edges where the target is already in the match-graph
                    (remove (fn [[n _]] (= node n))) ; remove any potential self-references
                    (first))
          [node' edge-weight] edge]

; if the current node is already a member of the match-graph, 
; or if there is no edge: continue
      (if (or (contains? (:nodeset match-graph) node)
              (nil? edge))
        (match-unsorted tail graph match-graph weight)

; else add the current node, the discovered edge and its target to the match graph. 
; adjust the total weight. then continue
        (let [match-graph' (-> match-graph
                               (loom/add-nodes node node')
                               (loom/add-edges [node node' edge-weight]))
 ; remove the target of the edge from the node-list
              tail' (remove (fn [[n _]] (= node' n)) tail)
              weight' (+ weight edge-weight)]
          (match-unsorted tail' graph match-graph' weight'))))))


(defn- match-sorted [graph]
  (println "sorted matching makes no sense in a complete graph.")
  [])

(defn match 
; create a graph-matching as a starting point for the blossom algorithm
[graph & {:keys [sort] :or {sort false}}]
  (if sort
    (match-sorted graph)
    ; else
    (let [nodes (:nodeset graph)
          match-graph (loom/weighted-graph)
          weight 0]
      (match-unsorted nodes graph match-graph weight))))