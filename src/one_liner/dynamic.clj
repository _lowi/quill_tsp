(ns one-liner.dynamic
  (:require [quil.core :as q])
  (:require [clojure.core.matrix :as µ]
            [clojure.core.matrix :as µo])
  (:require [one-liner.threshold :as t]
            [one-liner.pixels :as p]
            [one-liner.sample :as s]
            [one-liner.graph.core :as graph]
            [one-liner.util :as util])
  (:require [one-liner.graph.matching.greedy-maximum-cardinality-matching :as greedy-matching])
  ) 
  
(def width 800)
(def height 800)
(def bands 2)
(def num-samples 128)

(defn- render-graph [width height graph]
  (q/background 255)
      ; nodes
  (q/fill 0)
  (q/no-stroke)
  (q/ellipse-mode :center)
  (doseq [[x y] (:nodeset graph)]
    (q/ellipse x y 8 8))
      ; edges
  (q/no-fill)
  (q/stroke 0)
  (q/stroke-weight 0.5)
  (doseq [[vertex neighbours] (:adj graph)]
    (doseq [[neighbour distance] neighbours]
      (q/line vertex neighbour))))

(defn- render-threshold-bands [bands]
  (doseq [[idx band] (map-indexed vector bands)]
    (let [g (apply q/create-graphics (µ/shape band))
          c (q/color (q/map-range idx 0 (count bands) 0 255) 255 (q/map-range idx 0 (count bands) 0 255) 128)]
      ; side-effect
      ; using map here, although we're not mapping anything
      ; this is due do me not finding an indexed iterator over a matrix.
      (q/with-graphics g
        (µ/emap-indexed (fn [index value]
                          (let [[y x] index]
                            (if (== 1 value) (q/set-pixel x y c))))
                        band))
      ; render the combined image
      (q/image g 0 0 800 800))))

(defn calculate-band [state]
  (let [[h & t] (get-in state [:threshold :levels])
        prev-threshold-matrix (last (get-in state [:threshold :matrices]))
        threshold-matrix (p/make-threshold-matrix (:image state) h)
        threshold-matrices' (conj (get-in state [:threshold :matrices]) threshold-matrix)
        
        band (µ/sub threshold-matrix prev-threshold-matrix)
        threshold-bands' (conj (get-in state [:threshold :bands]) band)]
    
    ; (println "threshold-level " h)
    ; (println "prev-threshold-matrix" (µ/esum prev-threshold-matrix))
    ; (println "threshold-matrix" (µ/esum threshold-matrix))
    ; (println "band" (µ/esum band) (µ/shape band))
    ; (println "threshold-images" (count  (get-in state [:threshold :images])))
    ; (println "threshold-images'" (count threshold-images'))
    
    (if (empty? t)
      (-> state
          (assoc :mode :sample)
          (assoc-in  [:threshold :matrices] threshold-matrices')
          (assoc-in  [:threshold :bands] threshold-bands'))
      
      (-> state
          (assoc-in  [:threshold :levels] t)
          (assoc-in  [:threshold :matrices] threshold-matrices')
          (assoc-in  [:threshold :bands] threshold-bands')
          )
      )))

(defn- sample [state]
  (let [samples (s/sample (get-in state [:threshold :bands]) num-samples)]
    (-> state
        (assoc :mode :make-graph)
        (assoc :samples samples))))

(defn- make-graph [state]
  (let [g (graph/make-graph (:samples state))]
    ; (println "graph" g)
    (-> state
        (assoc :mode :initial-matching)
        (assoc :graph g))))

(defn- initial-matching [state]
  (let [matching-graph (greedy-matching/match (:graph state))]
    (println "initial-matching graph" matching-graph)
    (-> state
        (assoc :mode :done)
        (assoc :matching-graph matching-graph))))

(defn setup []
  (let [url (str "03.png")
        image (q/load-image url)
        g (q/create-graphics width height) ;create a correctly dimesioned graphics object
        _ (q/with-graphics g (q/image image 0 0 width height)) ;side-effect: render the loaded image with the correct dimensions
        threshold (p/initialize g width height bands)]

    ; initial state
    {:image g :mode :image-loaded :threshold threshold}))

(defn dupdate [state]
  (case (:mode state)
    ; initial state: 
    ; the image has been loaded, now get the first threshold band
    :image-loaded  (-> state
                       (assoc :mode :calculate-band)
                       (assoc :band-index 0))
    :calculate-band (calculate-band state)
    :sample (sample state)
    :make-graph (make-graph state)
    :initial-matching (initial-matching state)
    :done (do 
            (q/no-loop)
            state)
    state))

(defn draw [state]
  (println "state!" (:mode state))
  ; (q/image (:image state) 0 0 800 800)
  (q/background 255)
  ; (render-threshold-bands (get-in state [:threshold :bands]))
  ; (render-graph width height (:graph state))
  (render-graph width height (:matching-graph state))
  )
