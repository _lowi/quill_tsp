(ns one-liner.core
  (:require [quil.core :as q], 
            [quil.middleware :as m])
  (:require [one-liner.dynamic :as dynamic]))

(use 'clojure.core.matrix)
(set-current-implementation :vectorz)


(q/defsketch one-liner
  :title "hello world"
  :size [800 800]
  :setup dynamic/setup
  :draw dynamic/draw
  :update dynamic/dupdate
  ; :mouse-moved dynamic/mouse-moved
  :middleware [m/fun-mode])
