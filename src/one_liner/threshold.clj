(ns one-liner.threshold
  (:require [quil.core :as q])) 



(defn- threshold [graphics width height bands]
  (let [start      0
        end        1
        stepsize   (/ (- end start) bands)

        ; create the threshold filters
        thresholds (take (+ 1 bands) (range start (+ end stepsize) stepsize))

        filter-fn  (fn [threshold]
                     (let [dest (q/create-graphics width height)]
                        ; side-effect: render filtered image
                       (q/with-graphics dest
                         (q/image graphics 0 0 width height)
                         (apply q/display-filter [:threshold threshold]))
                        ; return dest
                       dest))]
    ; apply all filters
    (map filter-fn thresholds)))

(defn- diffs [graphics-vec width height]
  ; range begins with 1 as we skip the very fist input
  (let [indices (range 1 (count graphics-vec))]
    (map (fn [i]
           (let [prev (nth graphics-vec (- i 1))
                 curr (nth graphics-vec i)
                 diff (q/create-graphics width height)]
        ; side-effect: render the diff graphics
             (q/with-graphics diff
               (q/blend-mode :difference)
               (q/image prev 0 0 width height)
               (q/image curr 0 0 width height)
               (apply q/display-filter [:invert]))
        ; return the graphics
             diff))
         indices)))

(defn- pixels [img]
  (let [px (q/pixels img)
        black (q/color 0)
        blacks (remove #(== % -1) (map-indexed (fn [index color] (if (== color black) index -1)) px))]
    ; add some random
    (shuffle blacks)))

(defn calculate-bands [image width height bands]
  (let [orig (q/create-graphics width height)                         
        ; sideffect: render the image into the graphics
        _ (q/with-graphics orig (q/image image 0 0 width height)) 
        
        ; generate a binary image for each threshold level
        threshold-images (threshold orig width height, bands)
        
        ; pairwise subtraction of the threshold images
        ; the result is an array of images containing the pixels for a given threshold band
        diff-images (diffs threshold-images width height)
        
        ; extract the pixels form the diff-images
        diff-pixels (mapv #(pixels %) diff-images)
        
        ; calculate the probability for each band by stepping from 100% down to 0%
        ; ie. darker regions get assinged higher probabilities
        ; p(black) == 100 
        ; p(white) == 0
        stepsize (* -1.0 (/ 100 (- bands 1)))
        probablilty-bands (take bands (range 100 stepsize stepsize))
        
        combined (interleave probablilty-bands diff-pixels)
        partitioned (partition 2 combined)
        pixel-bands (map (fn [[p i]] (hash-map :probability p :indices i)) partitioned)
        ; pixel-probabilities (apply assoc {} combined)
        ]
    pixel-bands))